package aplicacion;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class AppTransaccion {
	static Connection con;

	public static void main(String[] args) {

		try {
			con = ConexionBD.getConexion();
			if (con != null) {
				DatabaseMetaData dm;
				dm = con.getMetaData();
				System.out.println("Soporta transacciones -> " + dm.supportsTransactions());
				System.out.println("Estado autocommit actual: " + con.getAutoCommit());
//				System.out.println("Nivel de aislamiento actual: " + con.getTransactionIsolation() + " -> " + Connection.TRANSACTION_REPEATABLE_READ);

				if (dm.supportsTransactions()) {
					realizaTransaccion();
				} else {
					System.out.println("Las transacciones no están soportadas...");
				}
				ConexionBD.cerrar();
			}
		} catch (SQLException e) {
			System.err.println("Error: " + e.getMessage());
		}

	}

	private static void realizaTransaccion() {
		int filasAfectadas;
		try (Statement st1 = con.createStatement();
				Statement st2 = con.createStatement();
				Statement st3 = con.createStatement();) {

			con.setAutoCommit(false);

			filasAfectadas = st1.executeUpdate(
					"UPDATE empresa.linfactura SET cantidad=cantidad+10 where linea=1 and factura=1");
			System.out.println("Actualización1: " + filasAfectadas + " filas afectadas");

			// Podemos provocar error manualmente:
			// throw new SQLException("error sql provocado...");
			// ó también, por ejemplo, poniendo un articulo que no existe
			filasAfectadas = st2
					.executeUpdate("UPDATE empresa.linfactura SET articulo = 1 where linea=1 and factura=1");
			filasAfectadas = st2.executeUpdate(
					"UPDATE empresa.linfactura SET cantidad=cantidad-5 where linea=1 and factura=1");
			System.out.println("Actualización2: " + filasAfectadas + " filas afectadas");

			filasAfectadas = st3.executeUpdate(
					"UPDATE empresa.linfactura SET cantidad=cantidad+1 where linea=1 and factura=1");
			System.out.println("Actualización3: " + filasAfectadas + " filas afectadas");

			con.commit();
			System.out.println("Fin transacción: todo OK");

		} catch (SQLException e) {
			System.err.println("Error en transacción... " + e.getMessage());
			try {
				System.out.println("Haciendo rollback...");
				con.rollback();
			} catch (SQLException ex) {
				System.err.println("Error en rollback... " + ex.getMessage());
			}
		} finally {
			try {
				System.out.println("Ponemos autocommit a true (automáticamente se haría commit)");
				con.setAutoCommit(true);
			} catch (SQLException ex) {
				System.err.println("Error activando autocommit true..." + ex.getMessage());
			}
		}
	}

}
