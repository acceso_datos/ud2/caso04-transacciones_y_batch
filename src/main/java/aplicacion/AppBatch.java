package aplicacion;

import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

// SEGÚN EL DRIVER USADO (mariadb, postgresql, mysql, ...) y la versión puede haber DIFERENTES 
// COMPORTAMIENTOS. Se debe observar detenidamente.
public class AppBatch {
	static Connection con;

	public static void main(String[] args) {

		try {
			con = ConexionBD.getConexion();
			if (con != null) {
				DatabaseMetaData dm = con.getMetaData();
				System.out.println("Soporta procesamiento batch -> " + dm.supportsBatchUpdates());
				if (dm.supportsBatchUpdates()) {
					realizaBatch();
				} else {
					System.out.println("El procesamiento por lotes no está soportado...");
				}
				ConexionBD.cerrar();
			}
		} catch (SQLException e) {
			System.err.println("Error: " + e.getMessage());
		}

	}

	private static void realizaBatch() {
		int[] result = null;
		try (Statement st = con.createStatement()) {

			con.setAutoCommit(false);

			st.addBatch(
					"insert into empresa.articulo(nombre, precio, codigo, grupo) values('HD 120G',120.0,'HD120',1)");

			// Podemos provocar error: poner grupo que no exista o una tabla que no
			// existe...
			st.addBatch(
					"insert into empresa.articulo(nombre, precio, codigo, grupo) values ('HD 160G',160.0,'HD160',1000)");

			st.addBatch("update empresa.articulo set precio = precio - 2 where precio > 100");

			result = st.executeBatch();
			System.out.println("Resultado ejecución batch: " + Arrays.toString(result));

			// provocando error fin conexion cerrada. Si ocurre: no realizará ninguna
			// actualización.
			// ConexionBD.cerrar();
			con.commit();
			System.out.println("Operaciones confirmadas... Todo OK");
		} catch (BatchUpdateException ex) {
			System.out.print("Error Batch: ");
			int[] r = ex.getUpdateCounts();
			System.out.println(Arrays.toString(r));
//			System.out.println("-> Código de error: " + Statement.EXECUTE_FAILED);
			try {
				// ConexionBD.cerrar();
				// provocando error fin conexion cerrada. Si esto ocurre: podemos deshacer
				// cambios.
				con.commit();
				// con.rollback();
			} catch (SQLException ex1) {
				System.out.println("error haciendo commit/rollback1");
			}

		} catch (SQLException ex) {
			System.err.println("Error sql");
			try {
				// con.commit();
				con.rollback();
			} catch (SQLException ex1) {
				System.out.println("error haciendo commit/rollback2");
			}
		} finally {
			System.out.println("Fin lote");
			try {
				// Hace commit automaticamente
				con.setAutoCommit(true);
			} catch (SQLException ex) {
				System.out.println("error poniendo autocommit a true");
			}
		}
	}

}
